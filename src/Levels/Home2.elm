module Levels.Home2 exposing
  ( level
  )

import Dict

import Color
import Level
import Level.Position as Position
import Levels.F1
import Levels.E1
import Levels.End

level =
  { parentItem =
    ( 1
    , 1
    )
    , entranceDirection =
      Position.Right
    , statics =
      { terrain =
        Dict.fromList
        [ ( ( 6, 6 ), Level.Door Color.Purple )
        , ( ( 6, 7 ), Level.Door Color.Purple )
        , ( ( 6, 8 ), Level.Door Color.Purple )
        , ( ( 7, 6 ), Level.Door Color.Purple )
        , ( ( 8, 6 ), Level.Door Color.Purple )
        , ( ( 0, 4 ), Level.Plate )
        , ( ( 4, 0 ), Level.Plate )
        , ( ( 0, 0 ), Level.Wall )
        , ( ( 0, 1 ), Level.Wall )
        , ( ( 0, 3 ), Level.Wall )
        , ( ( 0, 2 ), Level.Wall )
        , ( ( 0, 5 ), Level.Wall )
        , ( ( 0, 6 ), Level.Wall )
        , ( ( 0, 7 ), Level.Wall )
        , ( ( 0, 8 ), Level.Wall )
        , ( ( 1, 0 ), Level.Wall )
        , ( ( 2, 0 ), Level.Wall )
        , ( ( 3, 0 ), Level.Wall )
        , ( ( 5, 0 ), Level.Wall )
        , ( ( 6, 0 ), Level.Wall )
        , ( ( 7, 0 ), Level.Wall )
        , ( ( 8, 0 ), Level.Wall )
        ]
      , text =
        [ "Welcome to the second half of the game!"
        ,  "It's purple this time."
        ]
      }
    , items =
      Dict.fromList
        [ ( ( 0, 4 )
          , Level.Realized
            Levels.F1.level
          )
        , ( ( 4, 0 )
          , Level.Realized
            Levels.E1.level
          )
        , ( ( 7, 7 )
          , Level.Realized Levels.End.level
          )
        ]
    , color =
    Color.Colorless
  }
