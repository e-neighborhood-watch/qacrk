module Subscriptions exposing
  ( subscriptions
  )

import Browser.Events
import Json.Decode
import Time


import Audio
import Level.Position as Position
import Model exposing
  ( Model
  )
import Message exposing
  ( Message
  )


keyboardSubscriptions : Sub Message.Keyboard
keyboardSubscriptions =
  let
    keyToMessage : String -> Message.Keyboard
    keyToMessage key =
      case
        key
      of
        "a" ->
          Message.Move Position.Left
        "d" ->
          Message.Move Position.Right
        "w" ->
          Message.Move Position.Up
        "s" ->
          Message.Move Position.Down
        "A" ->
          Message.MoveInto Position.Left
        "D" ->
          Message.MoveInto Position.Right
        "W" ->
          Message.MoveInto Position.Up
        "S" ->
          Message.MoveInto Position.Down
        "u" ->
          Message.Undo
        "z" ->
          Message.Undo
        _ ->
          Message.Noop
  in
    Json.Decode.string
    |> Json.Decode.field "key"
    |> Json.Decode.map keyToMessage
    |> Browser.Events.onKeyDown


subscriptions : Model -> Sub Message
subscriptions model =
  Sub.batch <|
    [ Sub.map Message.Audio Audio.subscriptions
    , Browser.Events.onVisibilityChange Message.VisibilityChanged
    ]
    ++
      if
        model.visible
      then
        [ Browser.Events.onAnimationFrame (Time.posixToMillis >> Message.TimePasses)
        , Sub.map Message.Keyboard keyboardSubscriptions
        ]
      else
        []
