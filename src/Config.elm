module Config exposing
  ( levelSize
  , audioStartRampDuration
  , audioTransitionDuration
  , audioMuteDuration
  , audioUnmuteDuration
  , playerMoveDuration
  , playerPushDuration
  , playerEnterDuration
  , undoMoveDuration
  , undoPushDuration
  , undoEnterDuration
  , hintFontSize
  , hintLineSpacing
  )


audioStartRampDuration : Float
audioStartRampDuration =
  1.2


audioTransitionDuration : Float
audioTransitionDuration =
  1.2


audioMuteDuration : Float
audioMuteDuration =
  0.6


audioUnmuteDuration : Float
audioUnmuteDuration =
  1.2


playerMoveDuration : Int
playerMoveDuration =
  100


playerPushDuration : Int
playerPushDuration =
  100


playerEnterDuration : Int
playerEnterDuration =
  100


undoMoveDuration : Int
undoMoveDuration =
  100


undoPushDuration : Int
undoPushDuration =
  100


undoEnterDuration : Int
undoEnterDuration =
  100


levelSize : number
levelSize =
  9

hintFontSize : Float
hintFontSize =
  0.2
  
hintLineSpacing : Float
hintLineSpacing =
  0.3
