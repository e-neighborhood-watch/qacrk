module Level exposing
  ( Level
  , Room
  , Terrain (..)
  , Item (..)
  , itemColor
  , move
  , moveInto
  , Action
  , rollback
  )


import Dict exposing
  ( Dict
  )
import Set exposing
  ( Set
  )

import Level.Position as Position exposing
  ( Position
  )


import Animation exposing
  ( Animation
  )
import Color exposing
  ( Color (..)
  )
import Config


type Terrain
  = Plate
  | Wall
  | Door Color

type alias Statics =
  { terrain :
    Dict Position Terrain
  , text :
    List String
  }

type alias GenericLevel a =
  { a
  | items :
    Dict Position Item
  , statics :
    Statics
  , color :
    Color
  }


type alias Level =
  GenericLevel
    { playerPos :
      Position
    , openDoors :
      Set Position
    }


type Item
  = Atomic Color
  | Realized Room


itemColor : Item -> Color
itemColor item =
  case
    item
  of
    Atomic color ->
      color

    Realized room ->
      room.color


type alias Room =
  GenericLevel -- Note that items should exclude the parent
    { parentItem :
      Position
    , entranceDirection :
      Position.Direction
    }


wallOrOutOfBounds : Level -> Position -> Bool
wallOrOutOfBounds { items, statics, color, openDoors } ( x, y ) =
  if
    x < 0
      || x >= Config.levelSize
      || y < 0
      || y >= Config.levelSize
  then
    True
  else
    case
      Dict.get ( x, y ) statics.terrain
    of
      Just Wall ->
        True

      Just (Door _) ->
        Set.member ( x, y ) openDoors
        |> not

      _ ->
        False


occupied : Level -> Position -> Bool
occupied level pos =
  wallOrOutOfBounds level pos
    || Dict.member pos level.items


recalculateRoomColor : Color -> Room -> Room
recalculateRoomColor parentColor prevRoom =
  if
    Dict.get prevRoom.parentItem prevRoom.statics.terrain
      /= Just Plate
  then
    prevRoom
  else
    let
      newColor : Color
      newColor =
        prevRoom.statics.terrain
          |> Dict.remove prevRoom.parentItem
          |>
            Dict.foldl
              ( \ pos statics color ->
                case
                  statics
                of
                  Wall ->
                    color

                  Door _ ->
                    color

                  Plate ->
                    case
                      Dict.get pos prevRoom.items
                    of
                      Nothing ->
                        Colorless
                      Just item ->
                        if
                          color == itemColor item
                        then
                          color
                        else
                          Colorless
              )
              parentColor
    in
      if
        newColor == prevRoom.color
      then
        prevRoom
      else
        { parentItem =
          prevRoom.parentItem
        , items =
          Dict.map
            ( \ _ item ->
              case
                item
              of
                Atomic _ ->
                  item

                Realized room ->
                  recalculateRoomColor newColor room
                    |> Realized
            )
            prevRoom.items
        , statics =
          prevRoom.statics
        , entranceDirection =
          prevRoom.entranceDirection
        , color =
          newColor
        }


recalculateColor : Level -> Level
recalculateColor prevLevel =
  let
    newColor : Color
    newColor =
      Dict.foldl
        ( \ pos statics color ->
          case
            statics
          of
            Wall ->
              color

            Door _ ->
              color

            Plate ->
              case
                Dict.get pos prevLevel.items
              of
                Nothing ->
                  Just Colorless
                Just item ->
                  case
                    color
                  of
                    Nothing ->
                      itemColor item
                        |> Just
                    Just prevColor ->
                      if
                        prevColor == itemColor item
                      then
                        Just prevColor
                      else
                        Just Colorless
        )
        Nothing
        prevLevel.statics.terrain
      |> Maybe.withDefault Colorless
  in
    if
      newColor == prevLevel.color
    then
      prevLevel
    else
      { playerPos =
        prevLevel.playerPos
      , items =
        Dict.map
          ( \ _ item ->
            case
              item
            of
              Atomic _ ->
                item

              Realized room ->
                recalculateRoomColor newColor room
                  |> Realized
          )
          prevLevel.items
      , statics =
        prevLevel.statics
      , color =
        newColor
      , openDoors =
        recalculateOpenDoors
          newColor
          prevLevel.playerPos
          prevLevel.items
          prevLevel.statics.terrain
      }


recalculateOpenDoors : Color -> Position -> Dict Position Item -> Dict Position Terrain -> Set Position
recalculateOpenDoors levelColor playerPos items =
  Dict.foldl
    ( \ pos terrain acc ->
      case
        terrain
      of
        Door doorColor ->
          if
            doorColor == levelColor
              || pos == playerPos
              || Dict.member pos items
          then
            Set.insert pos acc
          else
            acc
        _ ->
          acc
    )
    Set.empty


moveItem : Position -> Item -> Level -> Maybe Level
moveItem itemPos newItem prevLevel =
  if
    occupied prevLevel itemPos
  then
    Nothing
  else
    Just
      { playerPos =
        prevLevel.playerPos
      , items =
        Dict.insert
          itemPos
          newItem
          prevLevel.items
      , statics =
        prevLevel.statics
      , color =
        prevLevel.color
      , openDoors =
        prevLevel.openDoors
      }


offColorDoor : Position -> Color -> Dict Position Terrain -> Bool
offColorDoor pos levelColor terrain =
  case
    Dict.get pos terrain
  of
    Just (Door color) ->
      color /= levelColor

    _ ->
      True


move : Position.Direction -> Level -> Maybe ( Level, Action, Maybe Animation )
move moveDirection prevLevel =
  let
    moveOffset : Position
    moveOffset =
      Position.directionOffset moveDirection

    newPos : Position
    newPos =
      Position.translate
        prevLevel.playerPos
        moveOffset
  in
    if
      wallOrOutOfBounds prevLevel newPos
    then
      Nothing
    else
      case
        Dict.get newPos prevLevel.items
      of
        Nothing ->
          ( { playerPos =
              newPos
            , items =
              prevLevel.items
            , statics =
              prevLevel.statics
            , color =
              prevLevel.color
            , openDoors =
              if
                offColorDoor prevLevel.playerPos prevLevel.color prevLevel.statics.terrain
              then
                Set.remove prevLevel.playerPos prevLevel.openDoors
              else
                prevLevel.openDoors
            }
          , Move moveDirection
          , Animation.PlayerMove Config.playerMoveDuration moveDirection
            |> Just
          )
            |> Just

        Just item ->
          { playerPos =
            newPos
          , items =
            Dict.remove
              newPos
              prevLevel.items
          , statics =
            prevLevel.statics
          , color =
            prevLevel.color
          , openDoors =
            if
              offColorDoor prevLevel.playerPos prevLevel.color prevLevel.statics.terrain
            then
              Set.remove prevLevel.playerPos prevLevel.openDoors
            else
              prevLevel.openDoors
          }
            |>
              moveItem
                (Position.translate newPos moveOffset)
                item
            |>
              Maybe.map
                ( recalculateColor
                  >>
                    \ updatedLevel ->
                      ( updatedLevel
                      , if
                          updatedLevel.color
                            == prevLevel.color
                        then
                          Push moveDirection
                        else
                          PushWithColorChange moveDirection prevLevel.color prevLevel.items
                      , Animation.PlayerPush Config.playerPushDuration moveDirection newPos
                        |> Just
                      )
                )


moveInto : Position.Direction -> Level -> Maybe ( Level, Action, Maybe Animation )
moveInto moveDirection prevLevel =
  let
    moveOffset : Position
    moveOffset =
      Position.directionOffset moveDirection

    newPosition : Position
    newPosition =
        Position.translate moveOffset prevLevel.playerPos
  in
    if
      wallOrOutOfBounds prevLevel newPosition
    then
      Nothing
    else
      case
        Dict.get newPosition prevLevel.items
      of
        Nothing ->
          ( { playerPos =
              newPosition
            , items =
              prevLevel.items
            , statics =
              prevLevel.statics
            , color =
              prevLevel.color
            , openDoors =
              if
                offColorDoor prevLevel.playerPos prevLevel.color prevLevel.statics.terrain
              then
                Set.remove prevLevel.playerPos prevLevel.openDoors
              else
                prevLevel.openDoors
            }
          , Move moveDirection
          , Animation.PlayerMove Config.playerMoveDuration moveDirection
            |> Just
          )
            |> Just
        Just (Atomic color) ->
          ( { playerPos =
              ( 0, 1 )
            , items =
              Dict.fromList
               [ ( ( 0, 0 )
                 , { parentItem =
                     newPosition
                  , entranceDirection =
                    Position.reverseDirection moveDirection
                   , items =
                     Dict.remove
                       newPosition
                       prevLevel.items
                   , statics =
                     prevLevel.statics
                   , color =
                     prevLevel.color
                   }
                   |> Realized
                 )
               , ( (6, 6)
                 , Atomic color
                 )
               ]
            , statics =
              { terrain =
                Dict.fromList
                [ ( (6, 6)
                  , Plate
                  )
                ]
              , text =
                [ "There's not much here,"
                , "no matter how deep you go."
                ]
              }
            , color =
              color
            , openDoors =
              Set.empty
            }
          , Enter Position.Down moveDirection
          , Animation.PlayerEnter Config.playerEnterDuration Position.Down
            |> Just
          )
            |> Just

        Just (Realized destRoom) ->
          let
            destPosition : Position
            destPosition =
              destRoom.entranceDirection
                |> Position.directionOffset
                |> Position.translate destRoom.parentItem
          in
            let
              newItems : Dict Position Item
              newItems =
                Dict.insert
                  destRoom.parentItem
                  ( Realized
                    { parentItem =
                      newPosition
                    , entranceDirection =
                      Position.reverseDirection moveDirection
                    , items =
                      Dict.remove
                        newPosition
                        prevLevel.items
                    , statics =
                      prevLevel.statics
                    , color =
                      prevLevel.color
                    }
                  )
                  destRoom.items
            in
              ( { playerPos =
                  destPosition
                , items =
                  newItems
                , statics =
                  destRoom.statics
                , color =
                  destRoom.color
                , openDoors =
                  recalculateOpenDoors
                    destRoom.color
                    destPosition
                    newItems
                    destRoom.statics.terrain
                }
              , Enter destRoom.entranceDirection moveDirection
              , Animation.PlayerEnter Config.playerEnterDuration destRoom.entranceDirection
                |> Just
              )
                |> Just


type Action
  = Move Position.Direction
  | Push Position.Direction
  | PushWithColorChange Position.Direction Color (Dict Position Item)
  | Enter Position.Direction Position.Direction


{-
rollbackColor : Color -> Dict Position Terrain -> Dict Position Item -> Dict Position Item
rollbackColor targetColor statics =
  Dict.map
    ( \ position item ->
      if
        Dict.get position statics
          /= Just Plate
      then
        item
      else
        case
          item
        of
          Atomic color ->
            Atomic targetColor

          Realized room ->
            Realized <|
              if
                room.color
                  == targetColor
              then
                room
              else
                { parentItem =
                  room.parentItem
                , entranceDirection =
                  room.entranceDirection
                , statics =
                  room.statics
                , items =
                  rollbackColor targetColor room.statics room.items
                , color =
                  targetColor
                }
    )
-}


rollbackMove : Position.Direction -> Position -> Position
rollbackMove =
  Position.reverseDirectionOffset
    >> Position.translate


rollback : Action -> Level -> ( Level, Maybe Animation )
rollback action prevLevel =
  case
    action
  of
    Move moveDirection ->
      let
        destPosition : Position
        destPosition =
          rollbackMove moveDirection prevLevel.playerPos
      in
        ( { playerPos =
            destPosition
          , items =
            prevLevel.items
          , statics =
            prevLevel.statics
          , color =
            prevLevel.color
          , openDoors =
            if
              offColorDoor destPosition prevLevel.color prevLevel.statics.terrain
            then
              Set.insert destPosition prevLevel.openDoors
            else
              prevLevel.openDoors
          }
        , Animation.PlayerMove Config.undoMoveDuration (Position.reverseDirection moveDirection)
          |> Just
        )

    Push moveDirection ->
      let
        itemOldPos : Position
        itemOldPos =
          moveDirection
            |> Position.directionOffset
            |> Position.translate prevLevel.playerPos

        oldItem : Maybe Item
        oldItem =
          Dict.get
            itemOldPos
            prevLevel.items

        destPosition : Position
        destPosition =
          rollbackMove moveDirection prevLevel.playerPos
      in
        ( { playerPos =
            destPosition
          , items =
            prevLevel.items
              |> Dict.remove itemOldPos
              |> Dict.update prevLevel.playerPos ( \ _ -> oldItem )
          , statics =
            prevLevel.statics
          , color =
            prevLevel.color
          , openDoors =
            if
              offColorDoor destPosition prevLevel.color prevLevel.statics.terrain
            then
              Set.insert destPosition prevLevel.openDoors
            else
              prevLevel.openDoors
          }
        , Animation.PlayerUndoPush Config.undoPushDuration (Position.reverseDirection moveDirection) itemOldPos
          |> Just
        )

    PushWithColorChange moveDirection prevColor prevItems ->
      let
        destPosition : Position
        destPosition =
          rollbackMove moveDirection prevLevel.playerPos

        itemLocation : Position
        itemLocation =
          moveDirection
            |> Position.directionOffset
            |> Position.translate prevLevel.playerPos
      in
        ( { playerPos =
            destPosition
          , items =
            prevItems
          , statics =
            prevLevel.statics
          , color =
            prevColor
          , openDoors =
            recalculateOpenDoors
              prevColor
              destPosition
              prevItems
              prevLevel.statics.terrain
          }
      , Animation.PlayerUndoPush Config.undoPushDuration (Position.reverseDirection moveDirection) itemLocation
        |> Just
      )

    Enter comingOutDir entryDir ->
      let
        entrancePosition : Position
        entrancePosition =
          rollbackMove comingOutDir prevLevel.playerPos
      in
        case
          Dict.get entrancePosition prevLevel.items
        of
          Nothing ->
            ( prevLevel -- this should never happen
            , Nothing
            )

          Just item ->
            case
              item
            of
              Atomic _ ->
                ( prevLevel -- this should also never happen
                , Nothing
                )

              Realized room ->
                let
                  destPosition : Position
                  destPosition =
                    rollbackMove entryDir room.parentItem

                  roomItems : Dict Position Item
                  roomItems =
                    Dict.insert
                      room.parentItem
                      ( Realized
                        { parentItem =
                          entrancePosition
                        , entranceDirection =
                          comingOutDir
                        , items =
                          Dict.remove
                            entrancePosition
                            prevLevel.items
                        , statics =
                          prevLevel.statics
                        , color =
                          prevLevel.color
                        }
                      )
                      room.items
                in
                  ( { playerPos =
                      destPosition
                    , items =
                      roomItems
                    , statics =
                      room.statics
                    , color =
                      room.color
                    , openDoors =
                      recalculateOpenDoors
                        room.color
                        destPosition
                        roomItems
                        room.statics.terrain
                    }
                  , Animation.PlayerEnter Config.undoEnterDuration (Position.reverseDirection entryDir)
                    |> Just
                  )
