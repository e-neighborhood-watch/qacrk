module Image exposing
   ( Image (..)
   , toSrc
   , defs
   )


import Svg.Styled as Svg exposing
  ( Svg
  )
import Svg.Styled.Attributes as SvgAttr


import Color exposing
  ( Color (..)
  )
import DoorState exposing
  ( DoorState (..)
  )


type Image
  = Block Color
  | Floor
  | OpenDoor
  | Door Color
  | Wall Color
  | Player
  | Plate


all : List Image
all =
  [ List.map Block Color.all
  , [ Floor
    ]
  , [ OpenDoor
    ]
  , List.map Door Color.all
  , List.map Wall Color.all
  , [ Player
    ]
  , [ Plate
    ]
  ]
    |> List.concat


def : Image -> Svg msg
def image =
  let
    baseAttr : List (Svg.Attribute msg)
    baseAttr =
      [ defId image |> SvgAttr.id
      , SvgAttr.width "1"
      , SvgAttr.height "1"
      ]
  in
    if
      image == Floor
        || image == OpenDoor
    then
      Svg.use
        ( SvgAttr.xlinkHref (defSrc image ++ "#content") :: baseAttr )
        []
    else
      Svg.image
        ( SvgAttr.xlinkHref (defSrc image) :: baseAttr )
        []


defs : Svg msg
defs =
  all
    |> List.map def
    |> Svg.defs []


defId : Image -> String
defId image =
  case image of
    Player ->
      "player"
    Wall color ->
      "wall-" ++ Color.toString color
    Plate ->
      "platepad"
    Floor ->
      "floor"
    Door color ->
      "door-" ++ Color.toString color
    OpenDoor ->
      "open-door"
    Block color ->
      "block-" ++ Color.toString color


toSrc : Image -> String
toSrc image =
  "#" ++ defId image


defSrc : Image -> String
defSrc image =
  case image of
    Player ->
      getFile "player"
    Wall color ->
      getFile ("wall/" ++ Color.toString color)
    Plate ->
      getFile "platepad"
    Floor ->
      getFile "floor"
    Door color ->
      getFile ("door/" ++ Color.toString color)
    OpenDoor ->
      getFile "door/open"
    Block color ->
      getFile ("block/" ++ Color.toString color)

getFile : String -> String
getFile filename =
  {- ( if
    Config.debug.elmReactor
  then
    "/"
  else
    ""
  )
  ++ -} "assets/img/"
  ++ filename
  ++ ".svg"
