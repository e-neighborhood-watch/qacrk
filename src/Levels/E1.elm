module Levels.E1 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position
import Levels.E2

level =
  { parentItem =
    ( 0
    , 0
    )
  , entranceDirection =
    Position.Down
  , items =
    Dict.fromList
      [ ( ( 1, 3 )
        , Level.Atomic Color.Purple
        )
      , ( ( 1, 4 )
        , Level.Atomic Color.Purple
        )
      , ( ( 2, 5 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 5 )
        , Level.Atomic Color.Green
        )
      , ( ( 6, 1 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 7, 2 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 7, 4 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 1, 5 )
        , Level.Realized Levels.E2.level
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 4, 1 )
          , Level.Plate
          )
        , ( ( 5, 1 )
          , Level.Plate
          )
        , ( ( 4, 5 )
          , Level.Plate
          )
        , ( ( 7, 1 ), Level.Door Color.Green )
        , ( ( 6, 2 ), Level.Door Color.Green )
        ]
    , text =
      [ "Just a normal level, nothing to see here."
      ]
    }
  , color =
    Color.Colorless
  }
