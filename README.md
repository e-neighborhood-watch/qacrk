# Y-Sokombanator

*(submitted to [LVDVMDARE48](https://ldjam.com/events/ludum-dare/48/y-sokombanator))*

A game of sokoban where every pushable block is also a level. Push blocks onto plates so that every plate is occupied by a block of the same color, but if the blocks aren't the color you need, you will have to travel deeper into them and change their colors first.

If all plates are occupied by blocks of the same color, the level background will turn that color, allowing the level to be used as a block of that color in the level that contains it.

### Controls

Use WASD to move. Hold SHIFT while moving to enter blocks. Press U or Z to undo a move.

### Where to Play the Game

* Gitlab
  * [dev branch pipeline](https://gitlab.com/e-neighborhood-watch/qacrk/-/jobs/artifacts/dev/file/build/index.html?job=build) (bleeding edge, debugger on)
  * [pages](https://e-neighborhood-watch.gitlab.io/qacrk/) (most up to date stable release)
* [itch.io](https://e-neighborhood-watch.itch.io/y-sokombanator)
