module Draw exposing
  ( items
  , staticTerrain
  , doors
  , wall
  , character
  , backdrop
  , text
  )


import Css
import Dict exposing
  ( Dict
  )
import Set exposing
  ( Set
  )
import Svg.Styled as Svg exposing
  ( Svg
  )
import Svg.Styled.Attributes as SvgAttr
import Svg.Styled.Keyed as SvgKeyed
import Svg.Styled.Lazy as Svg


import Animation
import Color exposing
  ( Color
  )
import Config
import DoorState exposing
  ( DoorState
  )
import Image
import Level exposing
  ( Level
  )
import Level.Position as Position exposing
  ( Position
  )
import Message exposing
  ( Message
  )


image : Position -> Image.Image -> Svg Message
image (x, y) img =
  Svg.use
    [ x
      |> String.fromInt
      |> SvgAttr.x
    , y
      |> String.fromInt
      |> SvgAttr.y
    , SvgAttr.xlinkHref
      (Image.toSrc img)
    ]
    [ ]


item : Position -> Color -> Maybe Animation.Item -> Svg Message
item pos color animation =
  image pos (Image.Block color)
    |> List.singleton
    |>
      Svg.g
        [ Animation.interpolateItem animation
        ]


items : Dict Position Animation.Item -> Dict Position Level.Item -> Svg Message
items itemAnimations =
  Dict.map ( \ _ -> Level.itemColor )
    >>
      Dict.foldl
      ( \ pos color ->
        let
          renderedItem : ( String, Svg Message )
          renderedItem =
            ( String.fromInt (Tuple.first pos)
              ++ ","
              ++ String.fromInt (Tuple.second pos)
            , Dict.get pos itemAnimations
              |> Svg.lazy3 item pos color
            )
        in
          Dict.update
            (Color.toString color)
            ( \ prevEntry ->
              Just <|
                case
                  prevEntry
                of
                  Nothing ->
                    [ renderedItem
                    ]
                  Just otherItems ->
                    renderedItem :: otherItems
          )
      )
      Dict.empty
      >> Dict.map (\ _ -> SvgKeyed.node "g" [])
      >> Dict.toList
      >> SvgKeyed.node "g" []


wall : Color -> Position -> Svg Message
wall color pos =
  Image.Wall color
  |> image pos

character : Position -> Maybe Animation.Player -> Svg Message
character pos animation =
  image pos Image.Player
    |> List.singleton
    |>
      Svg.g
        [ Animation.interpolatePlayer animation
        ]


doors : Dict Position Level.Terrain -> Set Position -> Svg Message
doors terrainMap openDoors =
  terrainMap
    |> Dict.toList
    |>
      List.filterMap
        ( \ ( pos, terrain ) ->
          case
            terrain
          of
            Level.Door color ->
              Just <|
                if
                  Set.member pos openDoors
                then
                  image pos Image.OpenDoor
                    |> List.singleton
                    |>
                      Svg.g
                        [ SvgAttr.stroke (Color.floorStroke color)
                        , SvgAttr.fill (Color.light color)
                        ]
                else
                  Image.Door color
                    |> image pos
            _ ->
              Nothing
        )
    |> Svg.g []


staticTerrain : Dict Position Level.Terrain -> Color -> Svg Message
staticTerrain terrainMap color =
  List.concatMap
    ( \ x ->
      List.filterMap
        ( \ y ->
          case
            Dict.get (x, y) terrainMap
          of
            Nothing ->
              Svg.g
                [ SvgAttr.stroke (Color.floorStroke color)
                ]
                [ image ( x, y ) Image.Floor
                ]
                |> Just
            Just (Level.Door _) ->
              Nothing
            Just Level.Wall ->
              wall color (x, y)
                |> Just
            Just Level.Plate ->
              Image.Plate
                |> image (x, y)
                |> Just
        )
        ( List.range 0 (Config.levelSize - 1)
        )
    )
    ( List.range 0 (Config.levelSize - 1)
    )
    |> Svg.g []


backdrop : Color -> Svg Message
backdrop color =
  Svg.rect
    [ SvgAttr.x "-1"
    , SvgAttr.y "-1"
    , Config.levelSize
      + 2
      |> String.fromInt
      |> SvgAttr.width
    , Config.levelSize
      + 2
      |> String.fromInt
      |> SvgAttr.height
    , SvgAttr.fill
      ( Color.light color
      )
    ]
    [ ]


text : List String -> Svg Message
text lines =
  Svg.text_
    [ SvgAttr.css
      [ Css.fontSize
        (Css.px Config.hintFontSize)
      , Css.property "dominant-baseline" "central"
      ]
    ]        
    ( List.indexedMap 
      ( \ i line ->
        Svg.tspan
          [ Config.levelSize + 1.1
            |> String.fromFloat
            |> SvgAttr.x
          , 1 + (toFloat i * Config.hintLineSpacing)
            |> String.fromFloat
            |> SvgAttr.y
          ]
          [ Svg.text 
            line
          ]
      )
      lines
    )
