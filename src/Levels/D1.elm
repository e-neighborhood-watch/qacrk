module Levels.D1 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position
import Levels.D2
import Levels.D3


level =
  { parentItem =
    ( 1
    , 7
    )
  , entranceDirection =
    Position.Right
  , items =
    Dict.fromList
      [ ( ( 5, 7 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 5 )
        , Level.Atomic Color.Red
        )
      , ( ( 4, 1 )
        , Level.Realized
          Levels.D2.level
        )
      , ( ( 7, 7 )
        , Level.Realized
          Levels.D3.level
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 1, 1 )
          , Level.Plate
          )
        , ( ( 2, 2 )
          , Level.Plate
          )
        , ( ( 7, 7 )
          , Level.Plate
          )
        , ( ( 8, 6 ), Level.Door Color.Red )
        , ( ( 1, 2 ) , Level.Wall )
        , ( ( 1, 3 ) , Level.Wall )
        , ( ( 2, 3 ) , Level.Wall )
        , ( ( 4, 2 ) , Level.Wall )
        , ( ( 6, 1 ) , Level.Wall )
        , ( ( 6, 2 ) , Level.Wall )
        , ( ( 4, 6 ) , Level.Wall )
        , ( ( 4, 7 ) , Level.Wall )
        , ( ( 4, 8 ) , Level.Wall )
        , ( ( 0, 5 ) , Level.Wall )
        , ( ( 0, 6 ) , Level.Wall )
        , ( ( 0, 7 ) , Level.Wall )
        , ( ( 0, 8 ) , Level.Wall )
        , ( ( 1, 8 ) , Level.Wall )
        , ( ( 6, 6 ) , Level.Wall )
        , ( ( 6, 7 ) , Level.Wall )
        , ( ( 7, 6 ) , Level.Wall )
        ]
    , text =
      [ "In this level, there aren't enough green blocks."
      , "You will have to go deeper into some"
      , "other blocks and turn them green."
      , "The dark red square in the lower left is a door."
      , "It will open if the level matches its color."
      , "If you make a mistake, press U or Z to undo."
      ]
    }
  , color =
    Color.Colorless
  }
