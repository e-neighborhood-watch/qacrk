module Update exposing
  ( update
  )


import Browser.Events
import Dict


import Animation exposing
  ( Animation
  )
import Audio
import Level exposing
  ( Level
  )
import Level.Position as Position
import Message exposing
  ( Message
  )
import Model exposing
  ( Model
  )


keyboardUpdate : Message.Keyboard -> Model.Keyboard -> ( Model.Keyboard, Maybe Animation )
keyboardUpdate msg prevModel =
  case
    msg
  of
    Message.Move dir ->
      case
        Level.move dir prevModel.level
      of
        Nothing ->
          ( prevModel
          , Nothing
          )
        Just ( newLevel, action, animations ) ->
          ( { level =
              newLevel
            , history =
              action :: prevModel.history
            }
          , animations
          )

    Message.MoveInto dir ->
      case
        Level.moveInto dir prevModel.level
      of
        Nothing ->
          ( prevModel
          , Nothing
          )
        Just ( newLevel, action, animations ) ->
          ( { level =
              newLevel
            , history =
              action :: prevModel.history
            }
          , animations
          )

    Message.Undo ->
      case
        prevModel.history
      of
        [] ->
          ( prevModel
          , Nothing
          )

        lastAction :: rest ->
          let
            ( newLevel, animations ) =
              Level.rollback lastAction prevModel.level
          in
            ( { level =
                newLevel
              , history =
                rest
              }
            , animations
            )

    Message.Noop ->
      ( prevModel
      , Nothing
      )


update : Message -> Model -> ( Model, Cmd Message )
update msg prevModel =
  case
    msg
  of
    Message.Keyboard keyboardMsg ->
      let
        ( newModel, newAnimation ) =
          keyboardUpdate keyboardMsg prevModel.model
      in
        ( { model =
            newModel
          , audio =
            { playing =
              prevModel.audio.initialized /= Nothing
            , initialized =
              prevModel.audio.initialized
            , loadsRemaining =
              prevModel.audio.loadsRemaining
            }
          , animation =
            Animation.start newAnimation prevModel.animation
          , visible =
            prevModel.visible
          }
        , case
            prevModel.audio.initialized
          of
            Nothing ->
              Cmd.none
            Just init ->
              if
                prevModel.audio.playing
              then
                if
                  prevModel.model.level.color
                    == newModel.level.color
                then
                  Cmd.none
                else
                  Audio.switch init newModel.level.color
              else
                Audio.start init newModel.level.color
        )

    Message.Audio audioMsg ->
      ( { model =
          prevModel.model
        , audio =
          Audio.update audioMsg prevModel.audio
        , animation =
          prevModel.animation
        , visible =
          prevModel.visible
        }
      , Cmd.none
      )

    Message.TimePasses curMillis ->
      ( { model =
          prevModel.model
        , audio =
          prevModel.audio
        , animation =
          Animation.update curMillis prevModel.animation
        , visible =
          prevModel.visible
        }
      , Cmd.none
      )

    Message.VisibilityChanged newVisibility ->
      let
        newVisible : Bool
        newVisible =
          case
            newVisibility
          of
            Browser.Events.Hidden ->
              False

            Browser.Events.Visible ->
              True
      in
        ( { model =
            prevModel.model
          , audio =
            prevModel.audio
          , animation =
            prevModel.animation
          , visible =
            newVisible
          }
        , if
            newVisible
          then
            Audio.unmute
          else
            Audio.mute
        )
