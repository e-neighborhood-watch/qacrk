module Levels.C1 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 8
    , 4
    )
  , entranceDirection =
    Position.Left
  , items =
    Dict.fromList
      [ ( ( 4, 3 ), Level.Atomic Color.Colorless )
      , ( ( 4, 5 ), Level.Atomic Color.Green )
      , ( ( 5, 3 ), Level.Atomic Color.Colorless )
      , ( ( 5, 4 ), Level.Atomic Color.Colorless )
      , ( ( 5, 5 ), Level.Atomic Color.Green )
      , ( ( 6, 3 ), Level.Atomic Color.Colorless )
      , ( ( 6, 5 ), Level.Atomic Color.Green )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 2, 3 ), Level.Wall )
        , ( ( 2, 2 ), Level.Wall )
        , ( ( 2, 4 ), Level.Wall )
        , ( ( 2, 5 ), Level.Wall )
        , ( ( 3, 3 ), Level.Wall )
        , ( ( 4, 1 ), Level.Wall )
        , ( ( 4, 2 ), Level.Plate )
        , ( ( 4, 7 ), Level.Wall )
        , ( ( 5, 1 ), Level.Wall )
        , ( ( 5, 2 ), Level.Plate )
        , ( ( 5, 7 ), Level.Wall )
        , ( ( 6, 1 ), Level.Wall )
        , ( ( 6, 2 ), Level.Plate )
        , ( ( 6, 7 ), Level.Wall )
        , ( ( 7, 1 ), Level.Wall )
        , ( ( 7, 2 ), Level.Wall )
        , ( ( 7, 3 ), Level.Wall )
        , ( ( 8, 3 ), Level.Wall )
        , ( ( 8, 5 ), Level.Wall )
        ]
    , text =
      [ "The three light gray tiles with the small circles"
      , "on them are pressure plates."
      , "If all pressure plates in a room are occupied by"
      , "blocks of the same color,"
      , "the level will change to match that color."
      , "You can push blocks using WASD."
      , "If you make a mistake, press U or Z to undo."
      , "To complete this level, turn it green."
      ]
    }
  , color =
    Color.Colorless
  }
