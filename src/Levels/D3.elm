module Levels.D3 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 1
    , 1
    )
  , entranceDirection =
    Position.Up
  , items =
    Dict.fromList
      [ ( ( 3, 1 )
        , Level.Atomic Color.Green
        )
      , ( ( 4, 2 )
        , Level.Atomic Color.Red
        )
      , ( ( 5, 5 )
        , Level.Atomic Color.Red
        )
      , ( ( 2, 6 )
        , Level.Atomic Color.Green
        )
      , ( ( 7, 4 )
        , Level.Atomic Color.Colorless
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 4, 2 )
          , Level.Plate
          )
        , ( ( 1, 1 )
          , Level.Plate
          )
        , ( ( 3, 0 ) , Level.Wall )
        , ( ( 0, 0 ) , Level.Wall )
        , ( ( 6, 0 ) , Level.Wall )
        , ( ( 0, 2 ) , Level.Wall )
        , ( ( 2, 2 ) , Level.Wall )
        , ( ( 3, 2 ) , Level.Wall )
        , ( ( 3, 3 ) , Level.Wall )
        , ( ( 5, 2 ) , Level.Wall )
        , ( ( 5, 3 ) , Level.Wall )
        , ( ( 6, 3 ) , Level.Wall )
        , ( ( 0, 5 ) , Level.Wall )
        , ( ( 6, 5 ) , Level.Wall )
        , ( ( 7, 5 ) , Level.Wall )
        , ( ( 8, 3 ) , Level.Wall )
        , ( ( 8, 5 ) , Level.Wall )
        , ( ( 3, 5 ) , Level.Wall )
        , ( ( 3, 6 ) , Level.Wall )
        , ( ( 3, 7 ) , Level.Wall )
        , ( ( 3, 8 ) , Level.Wall )
        , ( ( 5, 0 ), Level.Door Color.Colorless )
        , ( ( 6, 2 ), Level.Door Color.Colorless )
        , ( ( 8, 4 ), Level.Door Color.Red )
        , ( ( 2, 5 ), Level.Door Color.Red )
        ]
    , text =
      [ "The level you just came from is still a level,"
      , "and therefore it is a block as well,"
      , "so you can push it around like any other block."
      ]
    }
  , color =
    Color.Red
  }
