module Color exposing
   ( Color (..)
   , all
   , toString
   , light
   , floorStroke
   )

type Color
  = Colorless
  | Red
  | Blue
  | Green
  | Purple
  | Yellow


all : List Color
all =
  [ Colorless
  , Red
  , Blue
  , Green
  , Purple
  , Yellow
  ]


light : Color -> String
light color =
  case
    color
  of
   Colorless ->
     "#adadad"
   Red ->
     "#ff9073"
   Blue ->
     "#6f5cff"
   Green ->
     "#7dff9b"
   Purple ->
     "#df88ff"
   Yellow ->
     "#ffec94"


floorStroke : Color -> String
floorStroke color =
  case
    color
  of
   Colorless ->
     "#999999"
   Red ->
     "#ff6840"
   Blue ->
     "#4229ff"
   Green ->
     "#00d431"
   Purple ->
     "#c41eff"
   Yellow ->
     "#efc500"


toString : Color -> String
toString color =
  case
    color
  of
    Colorless ->
      "colorless"
    Red ->
      "red"
    Blue ->
      "blue"
    Green ->
      "green"
    Purple ->
      "purple"
    Yellow ->
      "yellow"
