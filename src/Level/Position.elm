module Level.Position exposing
  ( Position
  , translate
  , toReal
  , Real
  , Direction (..)
  , directionOffset
  , reverseDirectionOffset
  , reverseDirection
  )


type alias Position =
  ( Int -- X
  , Int -- Y
  )


type alias Real =
  ( Float -- X
  , Float -- Y
  )


translate : Position -> Position -> Position
translate ( x1, y1 ) ( x2, y2 ) =
  ( x1 + x2
  , y1 + y2
  )


toReal : Position -> Real
toReal ( x, y ) =
  ( toFloat x
  , toFloat y
  )


type Direction
  = Up
  | Down
  | Left
  | Right


directionOffset : Direction -> Position
directionOffset direction =
  case
    direction
  of
    Up ->
      ( 0, -1 )

    Down ->
      ( 0, 1 )

    Left ->
      ( -1, 0 )

    Right ->
      ( 1, 0 )


reverseDirection : Direction -> Direction
reverseDirection direction =
  case
    direction
  of
    Up ->
      Down

    Down ->
      Up

    Left ->
      Right

    Right ->
      Left


reverseDirectionOffset : Direction -> Position
reverseDirectionOffset direction =
  case
    direction
  of
    Up ->
      ( 0, 1 )

    Down ->
      ( 0, -1 )

    Left ->
      ( 1, 0 )

    Right ->
      ( -1, 0 )
