module Animation exposing
  ( Model
  , Animation (..)
  , Player
  , Item
  , interpolatePlayer
  , interpolateItem
  , start
  , initial
  , update
  )

import Dict exposing
  ( Dict
  )

import Svg.Styled as Svg

import Svg.Styled.Attributes as SvgAttr


import Config
import Level.Position as Position exposing
  ( Position
  )


type alias Move =
  { dx :
    Float
  , dy :
    Float
  , duration :
    Int
  , currentTime :
    Int
  , endingTime :
    Int
  }


type alias Player =
  Move


type alias Item =
  Move


type alias Model =
  { player :
    Maybe Player
  , items :
    Dict Position Item
  , lastTime :
    Int
  }


type Animation
  = PlayerMove Int Position.Direction
  | PlayerEnter Int Position.Direction
  | PlayerPush Int Position.Direction Position
  | PlayerUndoPush Int Position.Direction Position


interpolateMove : Maybe Move -> Svg.Attribute msg
interpolateMove maybeAnimation =
  SvgAttr.transform <|
      case
        maybeAnimation
      of
        Nothing ->
          ""

        Just { dx, dy, duration, currentTime, endingTime } ->
          let
            completion : Float
            completion =
              toFloat (endingTime - currentTime)
                / toFloat duration
                |> min 1
          in
            "translate("
              ++ String.fromFloat ( dx * completion )
              ++ " "
              ++ String.fromFloat ( dy * completion )
              ++ ")"


interpolatePlayer : Maybe Player -> Svg.Attribute msg
interpolatePlayer =
  interpolateMove


interpolateItem : Maybe Item -> Svg.Attribute msg
interpolateItem =
  interpolateMove


start : Maybe Animation -> Model -> Model
start newAnimation prevModel =
  case
    newAnimation
  of
    Nothing ->
      prevModel
    Just (PlayerMove duration direction) ->
      let
        ( dx, dy ) =
          direction
            |> Position.reverseDirectionOffset 
      in
        { player =
          { endingTime =
            prevModel.lastTime + duration
          , dx =
            toFloat dx
          , dy =
            toFloat dy
          , duration =
            duration
          , currentTime =
            prevModel.lastTime
          }
            |> Just
          , items =
            prevModel.items
        , lastTime =
          prevModel.lastTime
        }
    Just (PlayerEnter duration direction) ->
      let
        ( dx, dy ) =
          direction
            |> Position.reverseDirectionOffset 
      in
        { player =
          { endingTime =
            prevModel.lastTime + duration
          , dx =
            toFloat dx
          , dy =
            toFloat dy
          , duration =
            duration
          , currentTime =
            prevModel.lastTime
          }
            |> Just
          , items =
            Dict.empty
        , lastTime =
          prevModel.lastTime
        }
    Just (PlayerPush duration direction prevPosition) ->
      let
        ( dx, dy ) =
          direction
            |> Position.reverseDirectionOffset 

        itemPosition : Position
        itemPosition =
          direction
            |> Position.directionOffset
            |> Position.translate prevPosition
      in
        { player =
          { endingTime =
            prevModel.lastTime + duration
          , dx =
            toFloat dx
          , dy =
            toFloat dy
          , duration =
            duration
          , currentTime =
            prevModel.lastTime
          }
            |> Just
          , items =
            prevModel.items
              |> Dict.remove prevPosition
              |>
                Dict.insert
                  itemPosition
                  { endingTime =
                    prevModel.lastTime + duration + ( duration // 5 )
                  , dx =
                    toFloat dx
                  , dy =
                    toFloat dy
                  , duration =
                    duration
                  , currentTime =
                    prevModel.lastTime
                  }
        , lastTime =
          prevModel.lastTime
        }
    Just (PlayerUndoPush duration direction prevPosition) ->
      let
        ( dx, dy ) =
          direction
            |> Position.reverseDirectionOffset 

        itemPosition : Position
        itemPosition =
          direction
            |> Position.directionOffset
            |> Position.translate prevPosition
      in
        { player =
          { endingTime =
            prevModel.lastTime + duration + ( duration // 5 )
          , dx =
            toFloat dx
          , dy =
            toFloat dy
          , duration =
            duration
          , currentTime =
            prevModel.lastTime
          }
            |> Just
          , items =
            prevModel.items
              |> Dict.remove prevPosition
              |>
                Dict.insert
                  itemPosition
                  { endingTime =
                    prevModel.lastTime + duration
                  , dx =
                    toFloat dx
                  , dy =
                    toFloat dy
                  , duration =
                    duration
                  , currentTime =
                    prevModel.lastTime
                  }
        , lastTime =
          prevModel.lastTime
        }
      


initial : Model
initial =
  { player =
    Nothing
  , items =
    Dict.empty
  , lastTime =
    0
  }


update : Int -> Model -> Model
update curMillis prevModel =
  { player =
    case
      prevModel.player
    of
      Nothing ->
        Nothing

      Just animation ->
        if
          animation.endingTime
            <= curMillis
        then
          Nothing
        else
          { endingTime =
            animation.endingTime
          , dx =
            animation.dx
          , dy =
            animation.dy
          , duration =
            animation.duration
          , currentTime =
            curMillis
          }
            |> Just
  , items =
    prevModel.items
      |>
        Dict.filter
          ( \ _ { endingTime } ->
            endingTime > curMillis
          )
      |>
        Dict.map
          ( \ _ { endingTime, dx, dy, duration } ->
            { endingTime =
              endingTime
            , dx =
              dx
            , dy =
              dy
            , duration =
              duration
            , currentTime =
              curMillis
            }
          )
  , lastTime =
    curMillis
  }
