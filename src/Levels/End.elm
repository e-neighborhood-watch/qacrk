module Levels.End exposing
  ( level
  )

import Dict

import Color
import Level
import Level.Position as Position

level =
  { parentItem =
    ( 1
    , 1
    )
    , entranceDirection =
      Position.Right
    , statics =
      { terrain =
        Dict.fromList
        [ ( ( 4, 4 ), Level.Wall )
        ]
      , text =
        [ "Congratulations!"
        , "This is the end of the game."
        ]
      }
    , items =
      Dict.fromList
        [ ( ( 6, 2 )
          , Level.Atomic Color.Purple
          )
        , ( ( 6, 4 )
          , Level.Atomic Color.Blue
          )
        , ( ( 6, 6 )
          , Level.Atomic Color.Green
          )
        , ( ( 4, 6 )
          , Level.Atomic Color.Yellow
          )
        , ( ( 2, 6 )
          , Level.Atomic Color.Red
          )
        ]
    , color =
    Color.Colorless
  }
