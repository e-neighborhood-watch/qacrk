module Main exposing
  ( main
  )


import Browser


import Model exposing
  ( Model
  )
import Message exposing
  ( Message
  )
import View exposing
  ( view
  )
import Update exposing
  ( update
  )
import Subscriptions exposing
  ( subscriptions
  )


main : Program () Model Message
main =
  Browser.document
    { init =
      Model.initial
    , update =
      update
    , subscriptions =
      subscriptions
    , view =
      \ model ->
        { title =
          "Something..."
        , body =
          view model
        }
    }
