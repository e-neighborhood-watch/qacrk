module Message exposing
  ( Message (..)
  , Keyboard (..)
  )


import Browser.Events


import Audio
import Level.Position as Position


type Message
  = Keyboard Keyboard
  | Audio Audio.Message
  | TimePasses Int
  | VisibilityChanged Browser.Events.Visibility


type Keyboard
  = Noop
  | Move Position.Direction
  | MoveInto Position.Direction
  | Undo
