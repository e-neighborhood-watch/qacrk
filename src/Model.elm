module Model exposing
  ( Model
  , Keyboard
  , initial
  )


import Dict
import Set exposing
  ( Set
  )


import Animation
import Audio
import Color
import Level exposing
  ( Level
  )
import Level.Position as Position
import Levels.A1
import Levels.B1
import Levels.C1
import Levels.D1
import Levels.F1
import Levels.Home2
import Message exposing
  ( Message
  )


type alias Keyboard =
  { level :
    Level
  , history :
    List Level.Action
  }


type alias Model =
  { model :
    Keyboard
  , audio :
    Audio.Model
  , animation :
    Animation.Model
  , visible :
    Bool
  }


initial : () -> ( Model, Cmd Message )
initial _ =
  let
    ( audioModel, audioCmd ) =
      Audio.initialize Color.all
  in
    ( { model =
        { level =
          { playerPos =
            ( 1
            , 1
            )
          , color =
            Color.Colorless
          , items =
            Dict.fromList
              [ ( ( 2, 0 )
                , Level.Realized
                  Levels.B1.level
                )
              , ( ( 6, 0 )
                , Level.Realized
                  Levels.A1.level
                )
              , ( ( 0, 2 )
                , Level.Realized
                  Levels.C1.level
                )
              , ( ( 0, 6 )
                , Level.Realized Levels.D1.level
                )
              , ( ( 7, 7 )
                , Level.Realized Levels.Home2.level
                )
              ]
          , statics =
            { terrain =
              Dict.fromList
              [ ( ( 6, 6 ), Level.Door Color.Green )
              , ( ( 6, 7 ), Level.Door Color.Green )
              , ( ( 6, 8 ), Level.Door Color.Green )
              , ( ( 7, 6 ), Level.Door Color.Green )
              , ( ( 8, 6 ), Level.Door Color.Green )
              , ( ( 0, 2 ), Level.Plate )
              , ( ( 0, 6 ), Level.Plate )
              , ( ( 2, 0 ), Level.Plate )
              , ( ( 6, 0 ), Level.Plate )
              , ( ( 0, 0 ), Level.Wall )
              , ( ( 0, 1 ), Level.Wall )
              , ( ( 0, 3 ), Level.Wall )
              , ( ( 0, 4 ), Level.Wall )
              , ( ( 0, 5 ), Level.Wall )
              , ( ( 0, 7 ), Level.Wall )
              , ( ( 0, 8 ), Level.Wall )
              , ( ( 1, 0 ), Level.Wall )
              , ( ( 3, 0 ), Level.Wall )
              , ( ( 4, 0 ), Level.Wall )
              , ( ( 5, 0 ), Level.Wall )
              , ( ( 7, 0 ), Level.Wall )
              , ( ( 8, 0 ), Level.Wall )
              ]
            , text =
              [ "Use WASD to move."
              , ""
              , "The four light gray squares in the wall are levels."
              , ""
              , "Hold SHIFT while moving to move into a level."
              , ""
              , "Once all the levels are solved, the green area"
              , "in the corner will open up and you can proceed."
              , ""
              , "Start by going into one of the levels near the corner."
              ]
            }
          , openDoors =
            Set.empty
          }
        , history =
          []
        }
        , audio =
          audioModel
        , animation =
          Animation.initial
        , visible =
          True
      }
    , audioCmd
    )


