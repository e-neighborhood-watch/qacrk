module Levels.E3 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 8
    , 8
    )
  , entranceDirection =
    Position.Left
  , items =
    Dict.fromList
      [ ( ( 1, 4 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 3, 7 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 6, 7 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 5, 2 )
        , Level.Atomic Color.Purple
        )
      , ( ( 6, 3 )
        , Level.Atomic Color.Purple
        )
      , ( ( 5, 4 )
        , Level.Atomic Color.Purple
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 4, 2 )
          , Level.Plate
          )
        , ( ( 4, 3 )
          , Level.Plate
          )
        , ( ( 4, 4 )
          , Level.Plate
          )
        , ( ( 4, 0 ) , Level.Wall )
        , ( ( 4, 1 ) , Level.Wall )
        , ( ( 2, 2 ) , Level.Wall )
        , ( ( 2, 3 ) , Level.Wall )
        , ( ( 2, 4 ) , Level.Wall )
        , ( ( 5, 5 ) , Level.Door Color.Colorless )
        , ( ( 7, 5 ) , Level.Wall )
        , ( ( 8, 5 ) , Level.Wall )
        , ( ( 0, 6 ) , Level.Wall )
        , ( ( 1, 6 ) , Level.Wall )
        , ( ( 2, 6 ) , Level.Wall )
        , ( ( 5, 6 ) , Level.Wall )
        , ( ( 6, 4 ), Level.Door Color.Yellow )
        , ( ( 6, 2 ), Level.Door Color.Yellow )
        ]
    , text =
      [ "I couldn't think of any text to put here."
      ]
    }
  , color =
    Color.Colorless
  }
