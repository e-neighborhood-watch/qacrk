module Levels.D2 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 0
    , 8
    )
  , entranceDirection =
    Position.Right
  , items =
    Dict.fromList
      [ ( ( 1, 4 )
        , Level.Atomic Color.Green
        )
      , ( ( 5, 4 )
        , Level.Atomic Color.Red
        )
      , ( ( 4, 4 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 2, 5 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 3 )
        , Level.Atomic Color.Red
        )
      , ( ( 3, 4 )
        , Level.Atomic Color.Yellow
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 1, 5 )
          , Level.Door Color.Red
          )
        , ( ( 1, 2 )
          , Level.Wall
          )
        , ( ( 2, 4 )
          , Level.Door Color.Red
          )
        , ( ( 3, 2 )
          , Level.Wall
          )
        , ( ( 3, 4 )
          , Level.Plate
          )
        , ( ( 3, 5 )
          , Level.Door Color.Colorless
          )
        , ( ( 3, 7 )
          , Level.Wall
          )
        , ( ( 4, 2 )
          , Level.Wall
          )
        , ( ( 4, 4 )
          , Level.Plate
          )
        , ( ( 4, 6 )
          , Level.Door Color.Yellow
          )
        , ( ( 5, 1 )
          , Level.Wall
          )
        , ( ( 5, 5 )
          , Level.Wall
          )
        , ( ( 6, 3 )
          , Level.Wall
          )
        , ( ( 6, 7 )
          , Level.Wall
          )
        , ( ( 7, 3 )
          , Level.Wall
          )
        , ( ( 7, 7 )
          , Level.Wall
          )
        ]
    , text =
      [ "Tiles with four small squares on them are open doors."
      , "If the level changes color, they will close."
      ]
    }
  , color =
    Color.Yellow
  }
