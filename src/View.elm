module View exposing
  ( view
  )


import Css
import Dict
import Html as ElmHtml
import Html.Styled as Html exposing
  ( Html
  )
import Html.Styled.Lazy as Html
import Set
import Svg.Styled as Svg exposing
  ( svg
  , Svg
  )
import Svg.Styled.Attributes as SvgAttr
import Svg.Styled.Keyed as SvgKeyed
import Svg.Styled.Lazy as Svg


import Animation
import Color exposing
  ( Color
  )
import Config
import Draw
import Image
import Level exposing
  ( Room
  )
import Level.Position as Position exposing
  ( Position
  )
import Message exposing
  ( Message
  )
import Model exposing
  ( Model
  )


wallBorder : Color -> Svg Message
wallBorder color =
  ( -1, -1 )
    :: ( -1, Config.levelSize )
    :: ( Config.levelSize, -1 )
    :: ( Config.levelSize, Config.levelSize )
    ::
      ( List.range 0 ( Config.levelSize - 1)
        |>
          List.concatMap
            ( \ ix ->
              [ ( -1, ix )
              , ( ix, -1 )
              , ( Config.levelSize, ix )
              , ( ix, Config.levelSize )
              ]
            )
      )
    |> List.map (Draw.wall color)
    |> Svg.g []


view : { a | model : Model.Keyboard, animation : Animation.Model } -> List (ElmHtml.Html Message)
view { model, animation } =
  let
    character : Svg Message
    character =
      Svg.lazy2
        Draw.character
        model.level.playerPos
        animation.player

    items : Svg Message
    items =
      Svg.lazy2
        Draw.items
        animation.items
        model.level.items

    staticTerrain : Svg Message
    staticTerrain =
      Svg.lazy2
        Draw.staticTerrain
        model.level.statics.terrain
        model.level.color

    backdrop : Svg Message
    backdrop =
      Svg.lazy
        Draw.backdrop
        model.level.color

    doors : Svg Message
    doors =
      Svg.lazy2
        Draw.doors
        model.level.statics.terrain
        model.level.openDoors

    text : Svg Message
    text =
      Svg.lazy
        Draw.text
        model.level.statics.text

    body : List (Html Message)
    body =
      [ svg
        [ SvgAttr.viewBox
          ( "-1 -1 "
          ++ String.fromInt ( Config.levelSize + 2)
          ++ " "
          ++ String.fromInt ( Config.levelSize + 2)
          )
        , SvgAttr.preserveAspectRatio "xMidYMid"
        , SvgAttr.css
          [ Css.position Css.absolute
          , Css.top Css.zero
          , Css.left Css.zero
          , Css.width (Css.pct 100)
          , Css.height (Css.pct 100)
          ]
        , SvgAttr.id "main-level"
        ]
        [ Image.defs
        , backdrop
        , Svg.lazy wallBorder model.level.color
        , staticTerrain
        , doors
        , items
        , character
        , text
        ]
      ]
  in
    List.map Html.toUnstyled body
