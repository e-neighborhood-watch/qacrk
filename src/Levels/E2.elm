module Levels.E2 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position
import Levels.E3

level =
  { parentItem =
    ( 6
    , 5
    )
  , entranceDirection =
    Position.Down
  , items =
    Dict.fromList
      [ ( ( 2, 1 )
        , Level.Atomic Color.Green
        )
      , ( ( 2, 2 )
        , Level.Atomic Color.Green
        )
      , ( ( 2, 3 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 4 )
        , Level.Atomic Color.Yellow
        )
      , ( ( 6, 1 )
        , Level.Atomic Color.Purple
        )
      , ( ( 7, 2 )
        , Level.Atomic Color.Purple
        )
      , ( ( 1, 7 )
        , Level.Realized Levels.E3.level
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 4, 1 )
          , Level.Plate
          )
        , ( ( 5, 1 )
          , Level.Plate
          )
        , ( ( 4, 5 )
          , Level.Plate
          )
        , ( ( 3, 0 ) , Level.Wall )
        , ( ( 3, 2 ) , Level.Wall )
        , ( ( 0, 3 ) , Level.Wall )
        , ( ( 0, 5 ) , Level.Wall )
        , ( ( 2, 5 ) , Level.Wall )
        , ( ( 8, 5 ) , Level.Wall )
        , ( ( 3, 6 ) , Level.Wall )
        , ( ( 4, 6 ) , Level.Wall )
        , ( ( 5, 6 ) , Level.Wall )
        , ( ( 7, 6 ) , Level.Wall )
        , ( ( 8, 6 ) , Level.Wall )
        , ( ( 8, 7 ) , Level.Wall )
        , ( ( 4, 8 ) , Level.Wall )
        , ( ( 8, 8 ) , Level.Wall )
        , ( ( 7, 1 ), Level.Door Color.Yellow )
        , ( ( 6, 2 ), Level.Door Color.Yellow )
        , ( ( 1, 5 ), Level.Door Color.Green )
        ]
    , text =
      [ "Where are you going to find enough yellow?"
      ]
    }
  , color =
    Color.Colorless
  }
