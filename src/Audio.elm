port module Audio exposing
  ( Message
  , Initialized
  , Model
  , initialize
  , start
  , switch
  , mute
  , unmute
  , subscriptions
  , update
  )


import Json.Decode as Decode
import Json.Encode as Encode


import Color exposing
  ( Color
  )
import Config


port toJs : Encode.Value -> Cmd msg
port fromJs : (Encode.Value -> msg) -> Sub msg


type Initialized =
  Init


type Message
  = LoadedFile
  | StartFailed
  | Noop Decode.Error


type alias Model =
  { playing :
    Bool
  , loadsRemaining :
    Int
  , initialized :
    Maybe Initialized
  }


initializeModel : Int -> Model
initializeModel loadsRemaining =
  { playing =
    False
  , loadsRemaining =
    loadsRemaining
  , initialized =
    Nothing
  }


type Action
  = Initialize
  | Start
  | Switch
  | Mute
  | Unmute


actionToString : Action -> String
actionToString action =
  case
    action
  of
    Initialize ->
      "initialize"

    Start ->
      "start"

    Switch ->
      "switch"

    Mute ->
      "mute"

    Unmute ->
      "unmute"


sendEncoded : Action -> Encode.Value -> Cmd a
sendEncoded action data =
  Encode.object
    [ ( "action"
      , actionToString action |> Encode.string
      )
    , ( "data"
      , data
      )
    ]
    |> toJs


initialize : List Color -> ( Model, Cmd a )
initialize colors =
  ( List.length colors |> initializeModel
  , colors
    |>
      List.map
        ( \ color ->
          let
            colorString : String
            colorString =
              Color.toString color
          in
            ( colorString
            , "assets/sound/" ++ colorString ++ ".ogg" |> Encode.string
            )
        )
    |> Encode.object
    |> sendEncoded Initialize
  )


start : Initialized -> Color -> Cmd a
start Init color =
  [ ( "source"
    , Color.toString color |> Encode.string
    )
  , ( "timeConstant"
    , Config.audioStartRampDuration / 3 |> Encode.float
    )
  ]
    |> Encode.object
    |> sendEncoded Start


switch : Initialized -> Color -> Cmd a
switch Init color =
  [ ( "source"
    , Color.toString color |> Encode.string
    )
  , ( "timeConstant"
    , Config.audioTransitionDuration / 3 |> Encode.float
    )
  ]
    |> Encode.object
    |> sendEncoded Switch


mute : Cmd a
mute =
  Config.audioMuteDuration
    / 3
    |> Encode.float
    |> sendEncoded Mute


unmute : Cmd a
unmute =
  Config.audioUnmuteDuration
    / 3
    |> Encode.float
    |> sendEncoded Unmute


decoder : Decode.Decoder Message
decoder =
  Decode.field "type" Decode.string
    |>
      Decode.andThen
        ( \ messageType ->
          case
            messageType
          of
            "loadedFile" ->
              LoadedFile
                |> Decode.succeed

            "startFailed" ->
              StartFailed
                |> Decode.succeed

            _ ->
              "unrecognized type: "
                ++ messageType
                |> Decode.fail
        )


subscriptions : Sub Message
subscriptions =
  fromJs
    ( \ value ->
      case
        Decode.decodeValue decoder value
      of
        Ok result ->
          result

        Err error ->
          Noop error
    )


update : Message -> Model -> Model
update msg prevModel =
  case
    msg
  of
    LoadedFile ->
      { playing =
        False
      , loadsRemaining =
        prevModel.loadsRemaining - 1
      , initialized =
        if
          prevModel.loadsRemaining <= 1
        then
          Just Init
        else
          Nothing
      }

    StartFailed ->
      { playing =
        False
      , loadsRemaining =
        prevModel.loadsRemaining
      , initialized =
        prevModel.initialized
      }

    Noop _ ->
      prevModel
