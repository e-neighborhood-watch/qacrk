module Levels.A2 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 0, 0 )
  , entranceDirection =
    Position.Down
  , items =
    Dict.fromList
      [ ( ( 3, 4 )
        , Level.Atomic Color.Blue
        )
      , ( ( 4, 3 )
        , Level.Atomic Color.Blue
        )
      , ( ( 4, 5 )
        , Level.Atomic Color.Blue
        )
      , ( ( 5, 4 )
        , Level.Atomic Color.Blue
        )
      , ( ( 2, 3 )
        , Level.Atomic Color.Green
        )
      , ( ( 2, 4 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 5 )
        , Level.Atomic Color.Green
        )
      , ( ( 5, 7 )
        , Level.Atomic Color.Green
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 3, 4)
          , Level.Plate
          )
        , ( ( 4, 3 )
          , Level.Plate
          )
        , ( ( 4, 5 )
          , Level.Plate
          )
        , ( ( 5, 4 )
          , Level.Plate
          )
        , ( ( 0, 4 )
          , Level.Wall
          )
        , ( ( 1, 4 )
          , Level.Wall
          )
        , ( ( 2, 2 )
          , Level.Wall
          )
        , ( ( 3, 2 )
          , Level.Wall
          )
        , ( ( 3, 7 )
          , Level.Wall
          )
        , ( ( 4, 6 )
          , Level.Wall
          )
        , ( ( 4, 7 )
          , Level.Wall
          )
        , ( ( 4, 8 )
          , Level.Wall
          )
        , ( ( 5, 2 )
          , Level.Wall
          )
        , ( ( 6, 3 )
          , Level.Wall
          )
        , ( ( 6, 4 )
          , Level.Wall
          )
        , ( ( 6, 5 )
          , Level.Wall
          )
        , ( ( 5, 3 )
          , Level.Door Color.Colorless
          )
        ]
    , text =
      [ "That dark gray square is a colorless door."
      , "It will open if the level becomes colorless."
      , "The level will be colorless if the boxes on"
      , "the pressure plates are not all the same color."
      ]
    }
  , color =
    Color.Blue
  }
