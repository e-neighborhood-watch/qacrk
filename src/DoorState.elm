module DoorState exposing
  ( DoorState (..)
  )
  
type DoorState
  = Open
  | Closed