module Levels.F1 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 0, 0 )
  , entranceDirection =
    Position.Down
  , items =
    Dict.fromList
      [ ( ( 3, 4 )
        , Level.Atomic Color.Blue
        )
      , ( ( 4, 3 )
        , Level.Atomic Color.Blue
        )
      , ( ( 4, 5 )
        , Level.Atomic Color.Blue
        )
      , ( ( 5, 4 )
        , Level.Atomic Color.Blue
        )
      , ( ( 2, 3 )
        , Level.Atomic Color.Purple
        )
      , ( ( 2, 4 )
        , Level.Atomic Color.Purple
        )
      , ( ( 5, 7 )
        , Level.Atomic Color.Purple
        )
      , ( ( 6, 3 )
        , Level.Atomic Color.Purple
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 3, 4)
          , Level.Plate
          )
        , ( ( 4, 3 )
          , Level.Plate
          )
        , ( ( 4, 5 )
          , Level.Plate
          )
        , ( ( 5, 4 )
          , Level.Plate
          )
        , ( ( 0, 4 )
          , Level.Wall
          )
        , ( ( 1, 4 )
          , Level.Wall
          )
        , ( ( 1, 5 )
          , Level.Wall
          )
        , ( ( 1, 6 )
          , Level.Wall
          )
        , ( ( 2, 2 )
          , Level.Wall
          )
        , ( ( 2, 6 )
          , Level.Wall
          )
        , ( ( 2, 7 )
          , Level.Wall
          )
        , ( ( 2, 8 )
          , Level.Wall
          )
        , ( ( 3, 0 )
          , Level.Wall
          )
        , ( ( 3, 2 )
          , Level.Wall
          )
        , ( ( 3, 8 )
          , Level.Wall
          )
        , ( ( 4, 6 )
          , Level.Wall
          )
        , ( ( 4, 7 )
          , Level.Wall
          )
        , ( ( 4, 8 )
          , Level.Wall
          )
        , ( ( 5, 0 )
          , Level.Wall
          )
        , ( ( 5, 2 )
          , Level.Wall
          )
        , ( ( 6, 2 )
          , Level.Wall
          )
        , ( ( 7, 2 )
          , Level.Wall
          )
        , ( ( 7, 5 )
          , Level.Wall
          )
        , ( ( 7, 6 )
          , Level.Wall
          )
        , ( ( 7, 7 )
          , Level.Wall
          )
        , ( ( 7, 8 )
          , Level.Wall
          )
        , ( ( 8, 2 )
          , Level.Wall
          )
        , ( ( 8, 3 )
          , Level.Wall
          )
        , ( ( 8, 4 )
          , Level.Wall
          )
        , ( ( 8, 5 )
          , Level.Wall
          )
        , ( ( 4, 4 )
          , Level.Door Color.Colorless
          )
        ]
    , text =
      [ "Sometimes puzzles involve a little lateral thinking."
      , "Use the gimmick to your advantage."
      ]
    }
  , color =
    Color.Blue
  }
