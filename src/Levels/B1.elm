module Levels.B1 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 7
    , 4
    )
  , entranceDirection =
    Position.Left
  , items =
    Dict.fromList
      [ ( ( 3, 6 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 5 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 3 )
        , Level.Atomic Color.Green
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 4, 3 )
          , Level.Plate
          )
        , ( ( 4, 4 )
          , Level.Plate
          )
        , ( ( 5, 4 )
          , Level.Plate
          )
        , ( ( 1, 3 )
          , Level.Wall
          )
        , ( ( 1, 4 )
          , Level.Wall
          )
        , ( ( 1, 5 )
          , Level.Wall
          )
        , ( ( 1, 6 )
          , Level.Wall
          )
        , ( ( 2, 0 )
          , Level.Wall
          )
        , ( ( 2, 1 )
          , Level.Wall
          )
        , ( ( 2, 2 )
          , Level.Wall
          )
        , ( ( 2, 3 )
          , Level.Wall
          )
        , ( ( 2, 6 )
          , Level.Wall
          )
        , ( ( 2, 7 )
          , Level.Wall
          )
        , ( ( 2, 8 )
          , Level.Wall
          )
        , ( ( 3, 1 )
          , Level.Wall
          )
        , ( ( 3, 8 )
          , Level.Wall
          )
        , ( ( 4, 1 )
          , Level.Wall
          )
        , ( ( 4, 8 )
          , Level.Wall
          )
        , ( ( 5, 0 )
          , Level.Wall
          )
        , ( ( 5, 1 )
          , Level.Wall
          )
        , ( ( 5, 2 )
          , Level.Wall
          )
        , ( ( 5, 3 )
          , Level.Wall
          )
        , ( ( 5, 6 )
          , Level.Wall
          )
        , ( ( 5, 7 )
          , Level.Wall
          )
        , ( ( 5, 8 )
          , Level.Wall
          )
        , ( ( 6, 3 )
          , Level.Wall
          )
        , ( ( 6, 6 )
          , Level.Wall
          )
        , ( ( 7, 3 )
          , Level.Wall
          )
        , ( ( 7, 5 )
          , Level.Wall
          )
        , ( ( 7, 6 )
          , Level.Wall
          )
        , ( ( 8, 3 )
          , Level.Wall
          )
        , ( ( 8, 4 )
          , Level.Wall
          )
        , ( ( 8, 5 )
          , Level.Wall
          )
        ]
    , text =
      [ "The three light gray tiles with the small circles"
      , "on them are pressure plates."
      , "If all pressure plates in a room are occupied by"
      , "blocks of the same color,"
      , "the level will change to match that color."
      , "You can push blocks using WASD."
      , "If you make a mistake, press U or Z to undo."
      , "To complete this level, turn it green."
      ]
    }
  , color =
    Color.Colorless
  }
