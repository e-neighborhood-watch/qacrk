module Levels.A3 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position


level =
  { parentItem =
    ( 0, 0 )
  , entranceDirection =
    Position.Down
  , items =
    Dict.fromList
      [ ( ( 3, 3 )
        , Level.Atomic Color.Blue
        )
      , ( ( 3, 4 )
        , Level.Atomic Color.Blue
        )
      , ( ( 3, 5)
        , Level.Atomic Color.Blue
        )
      , ( ( 4, 4 )
        , Level.Atomic Color.Blue
        )
      , ( ( 2, 3 )
        , Level.Atomic Color.Green
        )
      , ( ( 2, 5)
        , Level.Atomic Color.Green
        )
      , ( ( 4, 6)
        , Level.Atomic Color.Green
        )
      , ( ( 5, 4)
        , Level.Atomic Color.Green
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 0, 3 )
          , Level.Wall
          )
        , ( ( 0, 4 )
          , Level.Wall
          )
        , ( ( 0, 5 )
          , Level.Wall
          )
        , ( ( 0, 6 )
          , Level.Wall
          )
        , ( ( 1, 6 )
          , Level.Wall
          )
        , ( ( 2, 0 )
          , Level.Wall
          )
        , ( ( 2, 1 )
          , Level.Wall
          )
        , ( ( 2, 6 )
          , Level.Wall
          )
        , ( ( 2, 7 )
          , Level.Wall
          )
        , ( ( 2, 8 )
          , Level.Wall
          )
        , ( ( 3, 1 )
          , Level.Wall
          )
        , ( ( 3, 3 )
          , Level.Plate
          )
        , ( ( 3, 4 )
          , Level.Plate
          )
        , ( ( 3, 5 )
          , Level.Plate
          )
        , ( ( 3, 8 )
          , Level.Wall
          )
        , ( ( 4, 4 )
          , Level.Plate
          )
        , ( ( 4, 5 )
          , Level.Door Color.Colorless
          )
        , ( ( 4, 7 )
          , Level.Wall
          )
        , ( ( 4, 8 )
          , Level.Wall
          )
        , ( ( 5, 1)
          , Level.Wall
          )
        , ( ( 5, 3 )
          , Level.Wall
          )
        , ( ( 5, 5 )
          , Level.Wall
          )
        , ( ( 5, 7 )
          , Level.Wall
          )
        , ( ( 6, 1 )
          , Level.Wall
          )
        , ( ( 7, 1 )
          , Level.Wall
          )
        , ( ( 7, 2 )
          , Level.Wall
          )
        ]
    , text =
      [ "That dark gray square is a colorless door."
      , "It will open if the level becomes colorless."
      ]
    }
  , color =
    Color.Blue
  }
