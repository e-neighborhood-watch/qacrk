module Levels.A1 exposing
  ( level
  )


import Dict


import Color
import Level
import Level.Position as Position
import Levels.A2
import Levels.A3


level =
  { parentItem =
    ( 6
    , 4
    )
  , entranceDirection =
    Position.Up
  , items =
    Dict.fromList
      [ ( ( 5, 2 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 2 )
        , Level.Atomic Color.Green
        )
      , ( ( 1, 3 )
        , Level.Realized
          Levels.A3.level
        )
      , ( ( 6, 1 )
        , Level.Realized
          Levels.A2.level
        )
      , ( ( 6, 2 )
        , Level.Atomic Color.Green
        )
      , ( ( 3, 5 )
        , Level.Atomic Color.Colorless
        )
      ]
  , statics =
    { terrain =
      Dict.fromList
        [ ( ( 3, 0 )
          , Level.Plate
          )
        , ( ( 4, 0 )
          , Level.Plate
          )
        , ( ( 5, 0 )
          , Level.Plate
          )
        , ( ( 6, 0 )
          , Level.Plate
          )
        , ( ( 8, 1 )
          , Level.Plate
          )
        , ( ( 0, 0 )
          , Level.Wall
          )
        , ( ( 0, 3 )
          , Level.Wall
          )
        , ( ( 0, 4 )
          , Level.Wall
          )
        , ( ( 0, 8 )
          , Level.Wall
          )
        , ( ( 1, 6 )
          , Level.Wall
          )
        , ( ( 2, 0 )
          , Level.Wall
          )
        , ( ( 2, 2 )
          , Level.Wall
          )
        , ( ( 2, 3 )
          , Level.Wall
          )
        , ( ( 2, 4 )
          , Level.Wall
          )
        , ( ( 2, 6 )
          , Level.Wall
          )
        , ( ( 3, 8 )
          , Level.Wall
          )
        , ( ( 4, 1 )
          , Level.Wall
          )
        , ( ( 4, 2 )
          , Level.Wall
          )
        , ( ( 4, 5 )
          , Level.Wall
          )
        , ( ( 4, 6 )
          , Level.Wall
          )
        , ( ( 5, 4 )
          , Level.Wall
          )
        , ( ( 5, 8 )
          , Level.Wall
          )
        , ( ( 6, 5 )
          , Level.Wall
          )
        , ( ( 7, 0 )
          , Level.Wall
          )
        , ( ( 7, 2 )
          , Level.Wall
          )
        , ( ( 7, 3 )
          , Level.Wall
          )
        , ( ( 7, 4 )
          , Level.Wall
          )
        , ( ( 7, 5 )
          , Level.Wall
          )
        , ( ( 7, 7 )
          , Level.Wall
          )
        ]
    , text =
      [ "In this level, there aren't enough green blocks."
      , ""
      , "You will have to go deeper into some other"
      , "blocks and turn them green."
      , ""
      , "If you make a mistake, press U or Z to undo."
      ]
    }
  , color =
    Color.Colorless
  }
